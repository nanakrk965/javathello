package model;

public final class Aspect {
	private int[][] board;
	private int currentTurn;
	private boolean isGameEnd;
	
	public Aspect(int[][] board, int currentTurn, boolean isGameEnd) {
		super();
		this.board = board;
		this.currentTurn = currentTurn;
		this.isGameEnd = isGameEnd;
	}

	public int[][] getBoard() {
		return this.board;
	}

	public int getCurrentTurn() {
		return this.currentTurn;
	}

	public boolean isGameEnd() {
		return this.isGameEnd;
	}
	
	

	
	
}
