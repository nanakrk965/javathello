package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Othello {

	static final int EMPTY_COORD = 0;
	public static final int BLACK_STONE = 1;
	static final int WHITE_STONE = 2;
	static final int LEGAL_COORD = 3;

	public static final int[][] INITIAL_BOARD = {
			{0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 3, 0, 0, 0, 0},
			{0, 0, 3, 2, 1, 0, 0, 0},
			{0, 0, 0, 1, 2, 3, 0, 0},
			{0, 0, 0, 0, 3, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0},
	};

	static final int[][] ALL_DIRECTIONS = {
		{-1, 0}, {1, 0}, {0, -1}, {0, 1},
		{-1, -1}, {-1, 1}, {1, -1}, {1, 1},
	};

	static final int[][] ALL_COORDS = {
				{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7},
				{1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4}, {1, 5}, {1, 6}, {1, 7},
				{2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4}, {2, 5}, {2, 6}, {2, 7},
				{3, 0}, {3, 1}, {3, 2}, {3, 3}, {3, 4}, {3, 5}, {3, 6}, {3, 7},
				{4, 0}, {4, 1}, {4, 2}, {4, 3}, {4, 4}, {4, 5}, {4, 6}, {4, 7},
				{5, 0}, {5, 1}, {5, 2}, {5, 3}, {5, 4}, {5, 5}, {5, 6}, {5, 7},
				{6, 0}, {6, 1}, {6, 2}, {6, 3}, {6, 4}, {6, 5}, {6, 6}, {6, 7},
				{7, 0}, {7, 1}, {7, 2}, {7, 3}, {7, 4}, {7, 5}, {7, 6}, {7, 7},
		};

	public static HashMap<Integer, Integer> opponentStoneColor() {
		HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();
		result.put(1, 2);
		result.put(2, 1);
		return result;
	}

	static final HashMap<Integer, Integer> OPPONENT_STONE_COLOR = opponentStoneColor();

	public static ArrayList<Integer> searchStoneLine(int[][] board, int row, int column, int[] direction) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		while (!(row == -1 || row == 8 || column == -1 || column == 8)) {
			result.add(board[row][column]);
			row += direction[0];
			column += direction[1];
		}
	    return result;
	}

	public static boolean isStoneLineOpponentStoneOnly(ArrayList<Integer> stoneLine, int opponentStoneColor) {
		for (int i = 0; i < stoneLine.size(); i++) {
			if (i == 0 || i == 1) {
				continue;
			}
			if (stoneLine.get(i) != opponentStoneColor) {
				return false;
			}

		}
		return true;
	}

	public static boolean isStoneLineEmptyFirst(ArrayList<Integer> stoneLine, int myStoneColor) {
		for (int i = 0; i < stoneLine.size(); i++) {
			if (i == 0 || i == 1) {
				continue;
			}
			if (stoneLine.get(i) == EMPTY_COORD || stoneLine.get(i) == LEGAL_COORD) {
				return true;
			}

			if (stoneLine.get(i) == myStoneColor) {
				return false;
			}
		}

		MyFunc.myAssert(false, "isEmptyFirstで強制終了");
		return false;
	}

	public static int stoneLineMyStoneFirstIndex(ArrayList<Integer> stoneLine, int myStoneColor) {
		for (int i = 0; i < stoneLine.size(); i++) {
			if (i == 0 || i == 1) {
				continue;
			}
			if (stoneLine.get(i) == myStoneColor) {
				return i;
			}
		}
		MyFunc.myAssert(false, "myStoneFirstIndexで強制終了");
		return 0;
	}

	public static int stoneLineReverseNum(ArrayList<Integer> stoneLine, int myStoneColor) {
		int opponentStoneColor = Othello.OPPONENT_STONE_COLOR.get(myStoneColor);

		if (stoneLine.size() < 3) {
			return 0;
		} else if (stoneLine.get(0) != EMPTY_COORD && stoneLine.get(0) != LEGAL_COORD) {
			return 0;
		} else if (stoneLine.get(1) != opponentStoneColor) {
			return 0;
		}

		if (isStoneLineOpponentStoneOnly(stoneLine, opponentStoneColor)) {
			return 0;
		}

		if (isStoneLineEmptyFirst(stoneLine, myStoneColor)) {
			return 0;
		}

		return stoneLineMyStoneFirstIndex(stoneLine, myStoneColor) - 1;
	}

	public static int[] rowLineBoardCopy(int[] rowLineBoard) {
		int[] result = new int[8];
		for (int i = 0; i < rowLineBoard.length; i++) {
			result[i] = rowLineBoard[i];
		}
		return result;
	}

	public static int[][] boardCopy(int[][] board) {
		int[][] result = new int[8][8];
		for (int i = 0; i < board.length; i++) {
			result[i] = rowLineBoardCopy(board[i]);
		}
		return result;
	}

	public static ArrayList<ArrayList<Integer>> eachDirectionReverseCoords(int row, int column, int[] direction, int reverseNum) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<>();
		for (int i = 0; i < reverseNum; i++) {
			row += direction[0];
			column += direction[1];
			result.add(new ArrayList<Integer>(Arrays.asList(row, column)));
		}
		return result;
	}

	public static ArrayList<ArrayList<Integer>> allDirectionReverseCoords(int[][] board, int row, int column, int myStoneColor) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		for (int[] direction : ALL_DIRECTIONS) {
			ArrayList<Integer> stoneLine = searchStoneLine(board, row, column, direction);
			int reverseNum = stoneLineReverseNum(stoneLine, myStoneColor);
			result.addAll(eachDirectionReverseCoords(row, column, direction, reverseNum));
		}
		return result;
	}

	public static int[][] reverse(int[][] tmpBoard, int row, int column, int myStoneColor) {
		int[][] board = boardCopy(tmpBoard);
		ArrayList<ArrayList<Integer>> reverseCoords = allDirectionReverseCoords(board, row, column, myStoneColor);
		for (ArrayList<Integer> coord : reverseCoords) {
			board[coord.get(0)][coord.get(1)] = myStoneColor;
		}
		return board;
	}

	public static int boardElementCount(int[][] board, int stone) {
		int result = 0;
		for (int[] coord : ALL_COORDS) {
			if (board[coord[0]][coord[1]] == stone) {
				result += 1;
			}
		}
		return result;
	}

	public static int blackStoneCount(int[][] board) {
		return boardElementCount(board, BLACK_STONE);
	}

	public static int whiteStoneCount(int[][] board) {
		return boardElementCount(board, WHITE_STONE);
	}

	public static int currentLegalCoordCount(int[][] board) {
		return boardElementCount(board, LEGAL_COORD);
	}

	public static ArrayList<ArrayList<Integer>> currentLegalCoords(int[][] board) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		for (int[] coord : ALL_COORDS) {
			int row = coord[0];
			int column = coord[1];
			if (board[row][column] == LEGAL_COORD) {
				result.add(new ArrayList<Integer>(Arrays.asList(row, column)));
			}
		}
		return result;
	}

	public static ArrayList<ArrayList<Integer>> nextLegalCoords(int[][] board, int myStoneColor) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		for (int[] coord : ALL_COORDS) {
			int row = coord[0];
			int column = coord[1];
			if (allDirectionReverseCoords(board, row, column, myStoneColor).size() != 0) {
				result.add(new ArrayList<Integer>(Arrays.asList(row, column)));
			}
		}
		return result;
	}

	public static int[][] updateLegalCoords(int[][] tmpBoard, int myStoneColor) {
		int[][] board = boardCopy(tmpBoard);
		for (ArrayList<Integer> coord : currentLegalCoords(board)) {
			board[coord.get(0)][coord.get(1)] = EMPTY_COORD;
		}

		for (ArrayList<Integer> coord : nextLegalCoords(board, myStoneColor)) {
			board[coord.get(0)][coord.get(1)] = LEGAL_COORD;
		}
		return board;
	}

	public static boolean isLegalCoord(int[][] board, int row, int column) {
		return board[row][column] == LEGAL_COORD;
	}

	public static boolean isGameEnd(int[][] board) {
		ArrayList<ArrayList<Integer>> blackNextLegalCoords = nextLegalCoords(board, BLACK_STONE);
		ArrayList<ArrayList<Integer>> whiteNextLegalCoords = nextLegalCoords(board, WHITE_STONE);
		return blackNextLegalCoords.size() == 0 && whiteNextLegalCoords.size() == 0;
	}

	public static final int DRAW = 0;

	public static int winner(int[][] board) {
		assert isGameEnd(board) : "ゲームが終了していない";

		int blackStoneNum = blackStoneCount(board);
		int whiteStoneNum = whiteStoneCount(board);
		if (blackStoneNum > whiteStoneNum) {
			return BLACK_STONE;
		} else if (blackStoneNum < whiteStoneNum) {
			return WHITE_STONE;
		} else {
			return DRAW;
		}
	}

	public static Aspect putStone(Aspect aspect, int row, int column) {
		assert isLegalCoord(aspect.getBoard(), row, column) : "合法ではない座標が渡された";

		int currentTurn = aspect.getCurrentTurn();
		int[][] board = reverse(aspect.getBoard(), row, column, currentTurn);

		board[row][column] = currentTurn;


		if (isGameEnd(board)) {
			board = updateLegalCoords(board, currentTurn);
			return new Aspect(board, currentTurn, true);
		}

		int opponentStoneColor = OPPONENT_STONE_COLOR.get(currentTurn);
		board = updateLegalCoords(board, opponentStoneColor);

		if (currentLegalCoordCount(board) != 0) {
			return new Aspect(board, opponentStoneColor, false);
		} else {
			board = updateLegalCoords(board, currentTurn);
			return new Aspect(board, currentTurn, false);
		}
	}

	public static String rowLineBoardToString(int[] rowLineBoard, int row) {
		String result = "" + row;
		for (int ele : rowLineBoard) {
			if (ele == EMPTY_COORD) {
				result += "□";
			} else if (ele == BLACK_STONE) {
				result += "●";
			} else if (ele == WHITE_STONE) {
				result += "◯";
			} else if (ele == LEGAL_COORD) {
				result += "×";
			} else {
				assert false;
			}
		}
		return result;
	}

	public static void printBoard(int[][] board) {
		System.out.println(" ０１２３４５６７");
		for (int i = 0; i < board.length; i++) {
			System.out.println(rowLineBoardToString(board[i], i));
		}
	}

	public static int rowOrColumnScanner(Scanner scanner, String msg) {
		int result = 0;
		while (true) {
			System.out.print(msg);
			try {
				int nextInt = scanner.nextInt();
				result = nextInt;
			} catch (InputMismatchException e) {
				System.out.println("数字以外が入力されたので、再度入力してクレメンス。");
				scanner.next();
				continue;
			}

			if (result >= 0 && result <= 7) {
				break;
			} else {
				System.out.println("0～7の範囲で入力してクレメンス 再度入力");
			}
		}
		return result;
	}

	public static int [] coordScanner(Aspect aspect, Scanner scanner) {
		int[] result = new int[2];
		while (true) {
			int row = rowOrColumnScanner(scanner, "x座標(0～7):");
			int column = rowOrColumnScanner(scanner, "y座標(0～7):");
			if (isLegalCoord(aspect.getBoard(), row, column)) {
				result = new int[] {row, column};
				break;
			} else {
				System.out.println("合法ではない座標が入力されたので、再度入力してクレメンス");
			}
		}
		return result;
	}
}

