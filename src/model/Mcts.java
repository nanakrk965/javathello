package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Mcts {
	public static double ucb(double averageValue, double totalSimuNum, double eachSimuNum, double X) {
		return averageValue + (X * Math.sqrt(2 * Math.log(totalSimuNum) / eachSimuNum));
	}

	public static int[] randomChoiceLegalCoord(int[][] board, Sfmt sfmt) {
		ArrayList<ArrayList<Integer>> currentLegalCoords = Othello.currentLegalCoords(board);
		int index = sfmt.NextInt(currentLegalCoords.size());
		ArrayList<Integer> currentLegalCoord = currentLegalCoords.get(index);
		return new int[] {currentLegalCoord.get(0), currentLegalCoord.get(1)};
	}


	public static double playOut(Aspect aspect, Sfmt sfmt, int myStoneColor) {
		while (!aspect.isGameEnd()) {
			int[] coord = randomChoiceLegalCoord(aspect.getBoard(), sfmt);
			aspect = Othello.putStone(aspect, coord[0], coord[1]);
		}

		int winner = Othello.winner(aspect.getBoard());
		if (winner == myStoneColor) {
			return 1.0;
		} else if (winner == Othello.OPPONENT_STONE_COLOR.get(myStoneColor)) {
			return 0.0;
		} else {
			return 0.5;
		}
	}

	public static double playOut(Aspect aspect, int row, int column, Sfmt sfmt, int myStoneColor) {
		aspect = Othello.putStone(aspect, row, column);

		while (!aspect.isGameEnd()) {
			int[] coord = randomChoiceLegalCoord(aspect.getBoard(), sfmt);
			aspect = Othello.putStone(aspect, coord[0], coord[1]);
		}

		int winner = Othello.winner(aspect.getBoard());
		if (winner == myStoneColor) {
			return 1.0;
		} else if (winner == Othello.OPPONENT_STONE_COLOR.get(myStoneColor)) {
			return 0.0;
		} else {
			return 0.5;
		}
	}

	public static ArrayList<Integer> rootNodeCoordCopy(ArrayList<Integer> coord) {
		 ArrayList<Integer> result = new ArrayList<>();
		 for (Integer ele : coord) {
			 result.add(ele);
		 }
		 return result;
	}

	public static ArrayList<ArrayList<Integer>> rootNodeKeyCopy(ArrayList<ArrayList<Integer>> rootNode) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<>();
		for (ArrayList<Integer> coord : rootNode) {
			result.add(rootNodeCoordCopy(coord));
		}
		return result;
	}

	public static boolean nextExpantionNodeExists(HashMap<ArrayList<ArrayList<Integer>>, Integer> totalSimuNumMap,
			                                      ArrayList<ArrayList<Integer>> currentRootNode) {
		for (int[] coord : Othello.ALL_COORDS) {
			ArrayList<ArrayList<Integer>> nextRootNode = rootNodeKeyCopy(currentRootNode);
			nextRootNode.add(new ArrayList<Integer>(Arrays.asList(coord[0], coord[1])));
			if (totalSimuNumMap.containsKey(nextRootNode)) {
				return true;
			}
		}
		return false;
	}
	public static HashMap<int[], Double> currentLegalCoordUcb(Aspect aspect,
			                                                  HashMap<ArrayList<ArrayList<Integer>>, Double> totalValueMap,
			                                                  HashMap<ArrayList<ArrayList<Integer>>, Integer> totalSimuNumMap,
			                                                  HashMap<ArrayList<ArrayList<Integer>>, Integer> eachSimuNumMap,
			                                                  ArrayList<ArrayList<Integer>> currentRootNode) {

		HashMap<int[], Double> result = new HashMap<>();
		ArrayList<ArrayList<Integer>> currentLegalCoords = Othello.currentLegalCoords(aspect.getBoard());
		for (ArrayList<Integer> coord : currentLegalCoords) {
			ArrayList<ArrayList<Integer>> nextRootNode = rootNodeKeyCopy(currentRootNode);
			nextRootNode.add(coord);

			double averageValue = totalValueMap.get(nextRootNode) / eachSimuNumMap.get(nextRootNode);
			int totalSimuNum = totalSimuNumMap.get(nextRootNode);
			int eachSimuNum = eachSimuNumMap.get(nextRootNode);
			result.put(new int[] {coord.get(0), coord.get(1)}, ucb(averageValue, totalSimuNum, eachSimuNum, 1));
		}
		return result;
	}

	public static int[] currentLegalMaxUcbCoord(Aspect aspect,
                                                HashMap<ArrayList<ArrayList<Integer>>, Double> totalValueMap,
                                                HashMap<ArrayList<ArrayList<Integer>>, Integer> totalSimuNumMap,
                                                HashMap<ArrayList<ArrayList<Integer>>, Integer> eachSimuNumMap,
                                                ArrayList<ArrayList<Integer>> currentRootNode) {

		HashMap<int[], Double> varCurrentLegalCoordUcb =
				currentLegalCoordUcb(aspect, totalValueMap, totalSimuNumMap, eachSimuNumMap, currentRootNode);
		int[] result = new int[2];

		for (int[] key : varCurrentLegalCoordUcb.keySet()) {
			result = key;
			break;
		}

		for (int[] key : varCurrentLegalCoordUcb.keySet()) {
			if (varCurrentLegalCoordUcb.get(key) > varCurrentLegalCoordUcb.get(result)) {
				result = key;
			}
		}
		return result;
	}

	public static int[] currentLegalMinUcbCoord(Aspect aspect,
                                                HashMap<ArrayList<ArrayList<Integer>>, Double> totalValueMap,
                                                HashMap<ArrayList<ArrayList<Integer>>, Integer> totalSimuNumMap,
                                                HashMap<ArrayList<ArrayList<Integer>>, Integer> eachSimuNumMap,
                                                ArrayList<ArrayList<Integer>> currentRootNode) {

		HashMap<int[], Double> varCurrentLegalCoordUcb =
				currentLegalCoordUcb(aspect, totalValueMap, totalSimuNumMap, eachSimuNumMap, currentRootNode);
		int[] result = new int[2];

		for (int[] key : varCurrentLegalCoordUcb.keySet()) {
			result = key;
			break;
		}

		for (int[] key : varCurrentLegalCoordUcb.keySet()) {
			if (varCurrentLegalCoordUcb.get(key) < varCurrentLegalCoordUcb.get(result)) {
				result = key;
			}
		}
		return result;
	}

	public static ArrayList<ArrayList<ArrayList<Integer>>> sameParentRootNodeKeys(HashMap<ArrayList<ArrayList<Integer>>, Integer> totalSimuNumMap,
			                                                                        ArrayList<ArrayList<Integer>> currentRootNode) {

		ArrayList<ArrayList<ArrayList<Integer>>> result = new ArrayList<>();
		for (int[] coord : Othello.ALL_COORDS) {
			ArrayList<ArrayList<Integer>> oneBeforeRootNode = rootNodeKeyCopy(currentRootNode);
			oneBeforeRootNode.remove(oneBeforeRootNode.size() - 1);
			oneBeforeRootNode.add(new ArrayList<Integer>(Arrays.asList(coord[0], coord[1])));
			if (totalSimuNumMap.containsKey(oneBeforeRootNode)) {
				result.add(oneBeforeRootNode);
			}
		}
		return result;
	}

	public static void totalSimuNumMapIncrement(HashMap<ArrayList<ArrayList<Integer>>, Integer> totalSimuNumMap,
			                                    ArrayList<ArrayList<Integer>> currentRootNode) {
		ArrayList<ArrayList<ArrayList<Integer>>> rootNodeKeys =
				sameParentRootNodeKeys(totalSimuNumMap, currentRootNode);
		for (ArrayList<ArrayList<Integer>> rootNode : rootNodeKeys) {
			totalSimuNumMap.put(rootNode, totalSimuNumMap.get(rootNode) + 1);
		}
	}

	public static void backwardAfterExpantion(HashMap<ArrayList<ArrayList<Integer>>, Double> totalValueMap,
			                                    ArrayList<ArrayList<Integer>> tmpEndRootNode,
			                                    double playOutValue) {

		for (int i = 0; i < tmpEndRootNode.size(); i++) {
			ArrayList<ArrayList<Integer>> endRootNode = rootNodeKeyCopy(tmpEndRootNode);
			for (int j = 0; j < i; j++) {
				endRootNode.remove(endRootNode.size() - 1);
			}
			totalValueMap.put(endRootNode, playOutValue);
		}
	}

	public static void backward(HashMap<ArrayList<ArrayList<Integer>>, Double> totalValueMap,
			                      ArrayList<ArrayList<Integer>> tmpEndRootNode,
			                      double playOutValue) {
		ArrayList<ArrayList<Integer>> endRootNode = rootNodeKeyCopy(tmpEndRootNode);
		for (int i = 0; i < endRootNode.size(); i++) {
			totalValueMap.put(endRootNode, totalValueMap.get(endRootNode) + playOutValue);
			endRootNode.remove(endRootNode.size() - 1);
		}
	}

	public static void printUcbParameter(HashMap<ArrayList<ArrayList<Integer>>, Double> totalValueMap,
                                           HashMap<ArrayList<ArrayList<Integer>>, Integer> totalSimuNumMap,
                                           HashMap<ArrayList<ArrayList<Integer>>, Integer> eachSimuNumMap) {
		for (ArrayList<ArrayList<Integer>> key : totalSimuNumMap.keySet()) {
			for (ArrayList<Integer> coord : key) {
				System.out.print("key:" + coord.get(0) + ":"+ coord.get(1) + "→");
			}
			System.out.println("totalValue=" + totalValueMap.get(key) + " totalSImu=" + totalSimuNumMap.get(key) + " eachSimu=" + eachSimuNumMap.get(key));
		}
	}

	public static int[] mcts(Aspect aspect, int simuNum, Sfmt sfmt) {
		HashMap<ArrayList<ArrayList<Integer>>, Double> totalValueMap = new HashMap<>();
		HashMap<ArrayList<ArrayList<Integer>>, Integer> totalSimuNumMap = new HashMap<>();
		HashMap<ArrayList<ArrayList<Integer>>, Integer> eachSimuNumMap = new HashMap<>();
		int myStoneColor = aspect.getCurrentTurn();

		ArrayList<ArrayList<Integer>> initialLegalCoords = Othello.currentLegalCoords(aspect.getBoard());
		ArrayList<ArrayList<ArrayList<Integer>>> initialRootNodeKeys = new ArrayList<>();
		for (ArrayList<Integer> coord : initialLegalCoords) {
			ArrayList<ArrayList<Integer>> expantionRootNode = new ArrayList<>();
			expantionRootNode.add(new ArrayList<Integer>(Arrays.asList(coord.get(0), coord.get(1))));
			initialRootNodeKeys.add(expantionRootNode);
			totalSimuNumMap.put(expantionRootNode, initialLegalCoords.size());
			eachSimuNumMap.put(expantionRootNode, 1);
			double playOutValue = playOut(aspect, coord.get(0), coord.get(1), sfmt, myStoneColor);
			totalValueMap.put(expantionRootNode, playOutValue);
		}

		int[] coord = new int[2];
		Aspect initialAspect = aspect;

		for (int i = 0; i < simuNum; i++) {
			ArrayList<ArrayList<Integer>> currentRootNode =  new ArrayList<>();
			aspect = initialAspect;
			int currentLegalNum = -1;
			while (nextExpantionNodeExists(totalSimuNumMap, currentRootNode)) {
				currentLegalNum = Othello.currentLegalCoordCount(aspect.getBoard());

			    if (aspect.getCurrentTurn() == myStoneColor) {
				    coord = currentLegalMaxUcbCoord(aspect, totalValueMap, totalSimuNumMap, eachSimuNumMap,
						                            currentRootNode);
			    } else {
				    coord = currentLegalMinUcbCoord(aspect, totalValueMap, totalSimuNumMap, eachSimuNumMap,
                                                    currentRootNode);
			    }

			    aspect = Othello.putStone(aspect, coord[0], coord[1]);
			    //Othello.printBoard(aspect.getBoard());
			    currentRootNode.add(new ArrayList<>(Arrays.asList(coord[0], coord[1])));
			    totalSimuNumMapIncrement(totalSimuNumMap, currentRootNode);
			    eachSimuNumMap.put(currentRootNode, eachSimuNumMap.get(currentRootNode) + 1);
			}

			assert currentLegalNum != -1;
			if ((currentLegalNum * 2) == eachSimuNumMap.get(currentRootNode)) {
				ArrayList<ArrayList<Integer>> currentLgCoords = Othello.currentLegalCoords(aspect.getBoard());
				for (ArrayList<Integer> lgCoord : currentLgCoords) {
					ArrayList<ArrayList<Integer>> expantionRootNode = rootNodeKeyCopy(currentRootNode);
					expantionRootNode.add(lgCoord);
					totalSimuNumMap.put(expantionRootNode, currentLgCoords.size());
					eachSimuNumMap.put(expantionRootNode, 1);

					double playOutValue = playOut(aspect, lgCoord.get(0), lgCoord.get(1), sfmt, myStoneColor);
					backwardAfterExpantion(totalValueMap, expantionRootNode, playOutValue);
				}
			} else {
				double playOutValue = playOut(aspect, sfmt, myStoneColor);
				backward(totalValueMap, currentRootNode, playOutValue);
			}
		}

		int[] result = new int[2];
		int maxEachSimuNum = 0;

		for (ArrayList<ArrayList<Integer>> rootNode : initialRootNodeKeys) {
			 if (maxEachSimuNum < eachSimuNumMap.get(rootNode)) {
				 result = new int[] {rootNode.get(0).get(0), rootNode.get(0).get(1)};
			 }
		}

		ArrayList<ArrayList<Integer>> key = new ArrayList<>();
		key.add(new ArrayList<>(Arrays.asList(result[0], result[1])));
		double averageValue = totalValueMap.get(key) / eachSimuNumMap.get(key);
		System.out.println("期待勝率:" + (averageValue * 100)+ "%");
		return result;
	}
}
