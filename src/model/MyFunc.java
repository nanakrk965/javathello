package model;

public class MyFunc {
	public static void myAssert(boolean bool, String errorMsg) {
		if (!bool) {
		    System.out.println(errorMsg);
		    System.exit(-1);
		}
	}
}
