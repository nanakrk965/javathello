package debug;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import model.Aspect;

public class Othello {
	public static void testStoneLineReverseNum() {
		ArrayList<Integer> stoneLine = new ArrayList<Integer>(Arrays.asList(0, 2, 2, 1));
		int result = model.Othello.stoneLineReverseNum(stoneLine, 1);
		assert result == 2 : result;

		stoneLine = new ArrayList<Integer>(Arrays.asList(0, 2, 2, 2));
		result = model.Othello.stoneLineReverseNum(stoneLine, 1);
		assert result == 0 : result;

		stoneLine = new ArrayList<Integer>(Arrays.asList(0, 2, 2, 3, 1));
		result = model.Othello.stoneLineReverseNum(stoneLine, 1);
		assert result == 0 : result;
		System.out.println("testStoneLineReverseNum成功");

	}

	public static boolean isLegalCoord(ArrayList<ArrayList<Integer>> legalCoords, int[] coord) {
		System.out.println("" + coord[0] + ":" + coord[1]);
		for (ArrayList<Integer> legalCoord : legalCoords) {
			if (legalCoord.get(0) == coord[0] && legalCoord.get(1) == coord[1]) {
				return true;
			}
		}
		return false;
	}

	public static void testAllDirectionReverseCoords() {
		int[][] board = {
				{0, 0, 0, 0, 0, 0, 0, 0},
				{0, 1, 0, 0, 0, 0, 0, 1},
				{0, 0, 2, 0, 1, 0, 1, 0},
				{0, 0, 0, 2, 2, 2, 0, 0},
				{1, 0, 2, 2, 0, 2, 2, 1},
				{0, 0, 0, 2, 2, 2, 0, 0},
				{0, 0, 2, 0, 1, 0, 3, 0},
				{0, 1, 0, 0, 2, 0, 0, 1},
		};

		ArrayList<ArrayList<Integer>> expectedReverseCoords = new ArrayList<ArrayList<Integer>>();
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(2, 2)));
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(3, 3)));
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(3, 4)));
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(3, 5)));
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(4, 5)));
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(4, 6)));
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(5, 3)));
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(5, 4)));
		expectedReverseCoords.add(new ArrayList<Integer>(Arrays.asList(6, 2)));

		ArrayList<ArrayList<Integer>> result = model.Othello.allDirectionReverseCoords(board, 4, 4, 1);
		assert result.size() == expectedReverseCoords.size() : "" + result.size() + ":" + expectedReverseCoords.size();

		for (ArrayList<Integer> coord : result) {
			String assertMsg = "" + coord.get(0) + ":"+ coord.get(1);
			assert isLegalCoord(expectedReverseCoords, new int[] {coord.get(0), coord.get(1)}) : assertMsg;
		}
		System.out.println("testAllDirectionReverseCoord成功");
	}

	public static void testNextLegalCoords() {
		int[][] board = {
				{0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 2, 0, 0, 0, 0},
				{0, 0, 2, 2, 0, 0, 0, 0},
				{0, 0, 0, 2, 1, 0, 0, 0},
				{0, 0, 0, 1, 1, 1, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0},
		};

		ArrayList<ArrayList<Integer>> expectedNextLegalCoords = new ArrayList<ArrayList<Integer>>();
		expectedNextLegalCoords.add(new ArrayList<Integer>(Arrays.asList(0, 3)));
		expectedNextLegalCoords.add(new ArrayList<Integer>(Arrays.asList(1, 1)));
		expectedNextLegalCoords.add(new ArrayList<Integer>(Arrays.asList(1, 2)));
		expectedNextLegalCoords.add(new ArrayList<Integer>(Arrays.asList(3, 2)));

		ArrayList<ArrayList<Integer>> result = model.Othello.nextLegalCoords(board, 1);
		for (ArrayList<Integer> coord : result) {
			assert isLegalCoord(expectedNextLegalCoords, new int[] {coord.get(0), coord.get(1)});
		}
		System.out.println("textNextLegalCoords成功");
	}

	public static void testIsGameEnd() {
		//最短のゲーム終了手順を正しく判定出来るかテスト
		Aspect aspect = new Aspect(model.Othello.INITIAL_BOARD, model.Othello.BLACK_STONE, false);
		aspect = model.Othello.putStone(aspect, 4, 5);
		aspect = model.Othello.putStone(aspect, 5, 3);
		aspect = model.Othello.putStone(aspect, 4, 2);
		aspect = model.Othello.putStone(aspect, 3, 5);
		aspect = model.Othello.putStone(aspect, 2, 4);
		aspect = model.Othello.putStone(aspect, 5, 5);
		aspect = model.Othello.putStone(aspect, 4, 6);
		aspect = model.Othello.putStone(aspect, 5, 4);
		aspect = model.Othello.putStone(aspect, 6, 4);
		assert aspect.isGameEnd();
		model.Othello.printBoard(aspect.getBoard());
		System.out.println("testIsGameEnd成功");
	}

	public static void testPutStone() {
		//パスが正しく出来ているかテスト
		Aspect aspect = new Aspect(model.Othello.INITIAL_BOARD, model.Othello.BLACK_STONE, false);

		aspect = model.Othello.putStone(aspect, 4, 5);
		assert aspect.getCurrentTurn() == 2;

		aspect = model.Othello.putStone(aspect, 5, 5);
		assert aspect.getCurrentTurn() == 1;

		aspect = model.Othello.putStone(aspect, 2, 3);
		assert aspect.getCurrentTurn() == 2;

		aspect = model.Othello.putStone(aspect, 4, 6);
		assert aspect.getCurrentTurn() == 1;

		aspect = model.Othello.putStone(aspect, 4, 7);
		assert aspect.getCurrentTurn() == 2;

		aspect = model.Othello.putStone(aspect, 3, 7);
		assert aspect.getCurrentTurn() == 1;

		aspect = model.Othello.putStone(aspect, 6, 5);
		assert aspect.getCurrentTurn() == 2;

		aspect = model.Othello.putStone(aspect, 5, 7);
		assert aspect.getCurrentTurn() == 2;

		model.Othello.printBoard(aspect.getBoard());
		System.out.println("↑黒石が置ける場所がない局面が表示されたら最短パスのテスト成功");

		//副作用が関数の外に漏れていないかテスト
		int[] seed = {(int) System.currentTimeMillis(), (int) Runtime.getRuntime().freeMemory()};
		model.Sfmt sfmt = new model.Sfmt(seed);

		Aspect intialAspect = new Aspect(model.Othello.INITIAL_BOARD, model.Othello.BLACK_STONE, false);
		aspect = intialAspect;

		while (!aspect.isGameEnd()) {
			int[] coord = model.Mcts.randomChoiceLegalCoord(aspect.getBoard(), sfmt);
			aspect = model.Othello.putStone(aspect, coord[0], coord[1]);
		}

		model.Othello.printBoard(intialAspect.getBoard());
		System.out.println("↑初期局面の状態が表示されれば testPutStoneの成功");
	}


	public static void testSample() {
		Aspect aspect = new Aspect(model.Othello.INITIAL_BOARD, model.Othello.BLACK_STONE, false);
		model.Othello.printBoard(aspect.getBoard());
		System.out.println( "現在の手番:" + aspect.getCurrentTurn());
		Scanner scanner = new Scanner(System.in);

		while (!aspect.isGameEnd()) {
			System.out.print("x座標:");
			int row = scanner.nextInt();
			System.out.println("y座標:");
			int column = scanner.nextInt();

			aspect = model.Othello.putStone(aspect, row, column);
			model.Othello.printBoard(aspect.getBoard());
			System.out.println( "現在の手番:" + aspect.getCurrentTurn());
		}
		scanner.close();
	}

	public static void main(String[] args) {
		testStoneLineReverseNum();
		testAllDirectionReverseCoords();
		testNextLegalCoords();
		testIsGameEnd();
		testPutStone();
		//testSample();
	}
}
