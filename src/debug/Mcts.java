package debug;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import model.Aspect;
import model.Othello;
import model.Sfmt;
//
public class Mcts {
	public static void testUCB() {
		System.out.println(model.Mcts.ucb(0.55, 10, 5, 1));
	}

	public static double multiPlayOut(Aspect aspect, int simuNum, Sfmt sfmt, int myStoneColor) {
		int totalValue = 0;
		for (int i = 0; i < simuNum; i++) {
			int[] coord = model.Mcts.randomChoiceLegalCoord(aspect.getBoard(), sfmt);
			totalValue += model.Mcts.playOut(aspect, coord[0], coord[1], sfmt, myStoneColor);
		}
		return totalValue / (double) simuNum;
	}

	public static void testPlayOut() {
		int[] seed = {(int) System.currentTimeMillis(), (int) Runtime.getRuntime().freeMemory()};
		Sfmt sfmt = new Sfmt(seed);
		int simuNum = 1000;

		//初期局面からのプレイアウトのテスト
		Aspect aspect = new Aspect(Othello.INITIAL_BOARD, Othello.BLACK_STONE, false);
		double averageValue = multiPlayOut(aspect, simuNum, sfmt, aspect.getCurrentTurn());
		System.out.println("約0.47になれば成功" + ":" + averageValue);


		//勝ち目がない局面からのプレイアウトのテスト
		int[][] board = new int[][] {
			{2, 2, 2, 2, 2, 2, 2, 2},
			{2, 0, 0, 0, 0, 0, 0, 2},
			{2, 0, 0, 3, 0, 0, 0, 2},
			{2, 0, 3, 2, 1, 0, 0, 2},
			{2, 0, 0, 1, 2, 3, 0, 2},
			{2, 0, 0, 0, 3, 0, 0, 2},
			{2, 0, 0, 0, 0, 0, 0, 2},
			{2, 2, 2, 2, 2, 2, 2, 2},
		};
		aspect = new Aspect(board, Othello.BLACK_STONE, false);
		averageValue = multiPlayOut(aspect, simuNum, sfmt, aspect.getCurrentTurn());
		System.out.println("約0.0になれば成功" + ":" + averageValue);
	}

	public static ArrayList<ArrayList<Integer>> newArrayList2d(int[][] array2d ) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<>();
		for (int[] array1d : array2d) {
			assert array1d.length == 2 : array1d.length;
			result.add(new ArrayList<Integer>(Arrays.asList(array1d[0], array1d[1])));
		}
		return result;

	}

	public static int mctsVsRandom() {
		int[] seed = {(int) System.currentTimeMillis(), (int) Runtime.getRuntime().freeMemory()};
		model.Sfmt sfmt = new model.Sfmt(seed);

		Aspect aspect = new Aspect(Othello.INITIAL_BOARD, Othello.BLACK_STONE, false);
		int[] coord = new int[2];

		while (true) {
		    if (aspect.getCurrentTurn() == Othello.BLACK_STONE) {
			    coord = model.Mcts.mcts(aspect, 32, sfmt);
		    } else {
			    coord = model.Mcts.randomChoiceLegalCoord(aspect.getBoard(), sfmt);
		    }
		    aspect = Othello.putStone(aspect, coord[0], coord[1]);
		    if (aspect.isGameEnd()) {
		    	System.out.println("isGameEnd");
			    return Othello.winner(aspect.getBoard());
		    }
		}
	}

	public static void testMcts() {
		int winCount = 0;
		int drawCount = 0;

		for (int i = 0; i < 100; i++) {
			int winner = mctsVsRandom();
			if (winner == Othello.BLACK_STONE) {
				winCount += 1;
			} else if (winner == Othello.DRAW) {
				drawCount += 1;
			}
			System.out.println(winCount);
		}
		System.out.println("勝ち数:" + winCount);
		System.out.println("引き分け数:" + drawCount);
	}

	public static void mctsVsPlayer() {
		int[] seed = {(int) System.currentTimeMillis(), (int) Runtime.getRuntime().freeMemory()};
		model.Sfmt sfmt = new model.Sfmt(seed);

		Aspect aspect = new Aspect(Othello.INITIAL_BOARD, Othello.BLACK_STONE, false);
		int[] coord = new int[2];

		Scanner scanner = new java.util.Scanner(System.in);

		while (true) {
		    if (aspect.getCurrentTurn() == Othello.BLACK_STONE) {
			    coord = model.Mcts.mcts(aspect, 512, sfmt);
		    } else {
		    	coord = Othello.coordScanner(aspect, scanner);
		    }
		    aspect = Othello.putStone(aspect, coord[0], coord[1]);
		    System.out.println("coord=" + coord[0] + ":" + coord[1]);
		    Othello.printBoard(aspect.getBoard());
		    if (aspect.isGameEnd()) {
		    	System.out.println("isGameEnd");
			    break;
		    }
		}
		scanner.close();
	}
	public static void main(String[] args) {
		//testUCB();
		//testPlayOut();
		//testMcts();
		mctsVsPlayer();
	}
}
