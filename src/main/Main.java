package main;

import java.util.InputMismatchException;
import java.util.Scanner;

import model.Aspect;
import model.Mcts;
import model.Othello;

public class Main {
	public static int selectPlayerColor(Scanner scanner) {
		int result = 0;
		System.out.print("先手後手を入力 1は先手(黒) 2は後手(白):");
		while (true) {
			try {
				result = scanner.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("数字以外が入力されたので、再度入力してクレメンス");
				scanner.next();
				continue;
			}
			if (result == 1 || result == 2) {
				break;
			} else {
				System.out.println("入力は1か2で入力してクレメンス 再度入力");
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		//メルセンヌツイスター
		int[] seed = {(int) System.currentTimeMillis(), (int) Runtime.getRuntime().freeMemory()};
		model.Sfmt sfmt = new model.Sfmt(seed);

		int playerColor = selectPlayerColor(scanner);
		int[] coord = new int[2];
		Aspect aspect = new Aspect(Othello.INITIAL_BOARD, Othello.BLACK_STONE, false);

		Othello.printBoard(aspect.getBoard());
		while (!aspect.isGameEnd()) {
			if (playerColor == aspect.getCurrentTurn()) {
				coord = Othello.coordScanner(aspect, scanner);
			} else {
				coord = Mcts.mcts(aspect, 16, sfmt);
			}
			aspect = Othello.putStone(aspect, coord[0], coord[1]);
			Othello.printBoard(aspect.getBoard());
		}

		int blackStoneNum = Othello.blackStoneCount(aspect.getBoard());
		int whiteStoneNum = Othello.whiteStoneCount(aspect.getBoard());
		System.out.println("黒の数:" + blackStoneNum);
		System.out.println("白の数:" + whiteStoneNum);

		int winner = Othello.winner(aspect.getBoard());
		if (winner == playerColor) {
			System.out.println("お前の勝ちや");
		} else {
			System.out.println("ワイの勝ちや");
		}
	}
}
